import {
  findAll,
  findOne,
  insertOne,
  replaceOne,
  deleteOne,
  findAllByParent,
} from './facade-template.js';

function ConsumptionFacade() {
  const facade = {};
  const collectionName = 'consumptions';

  /**
   * 
   * @param {*} userKey 
   * @param {*} pageSize 
   * @param {*} lastVisible 
   * @returns 
   */
  facade.findAllByUser = async (userKey) => {
    const path = `users/${userKey}/${collectionName}`;
    const result = findAll(path);
    return result;
  };

  /**
   * 
   * @param {*} userKey 
   * @param {*} groupKey 
   * @param {*} pageSize 
   * @param {*} lastVisible 
   * @returns 
   */
  facade.findAllByGroup = async (userKey, groupKey) => {
    const path = `users/${userKey}/${collectionName}`;
    const result = await findAllByParent(path, 'groupKey', groupKey);
    return result;
  };

  /**
   * 
   * @param {*} userKey 
   * @param {*} key 
   * @returns 
   */
  facade.findOne = async (userKey, key) => {
    const path = `users/${userKey}/${collectionName}`;
    const result = await findOne(path, key);
    return result;
  };

  /**
   * 
   * @param {*} userKey 
   * @param {*} document 
   * @returns 
   */
  facade.insertOne = async (userKey, document) => {
    const path = `users/${userKey}/${collectionName}`;
    const result = await insertOne(path, document);
    return result;
  };

  /**
   * 
   * @param {*} userKey 
   * @param {*} key 
   * @param {*} document 
   * @returns 
   */
  facade.replaceOne = async (userKey, key, document) => {
    const path = `users/${userKey}/${collectionName}`;
    const result = await replaceOne(path, key, document);
    return result;
  };

  /**
   * 
   * @param {*} userKey 
   * @param {*} key 
   * @returns 
   */
  facade.deleteOne = async (userKey, key) => {
    const path = `users/${userKey}/${collectionName}`;
    const result = await deleteOne(path, key);
    return result;
  };

  return facade;
}

const facade = ConsumptionFacade();
export default facade;
