import {
  findAll,
  findAllByParent,
  findOne,
  insertOne,
  replaceOne,
  deleteOne,
} from './facade-template.js';

function ProductFacade() {
  const facade = {};
  const collectionName = 'products';

  /**
   * 
   * @param {*} userKey 
   * @returns 
   */
  facade.findAllByUser = async (userKey) => {
    const path = `users/${userKey}/${collectionName}`;
    const result = await findAll(path);
    return result;
  };

  /**
   * 
   * @param {*} userKey 
   * @param {*} deviceKey 
   * @returns 
   */
  facade.findAllByDevice = async (userKey, deviceKey) => {
    const path = `users/${userKey}/${collectionName}`;
    const result = await findAllByParent(path, 'deviceKey', deviceKey);
    return result;
  };

  /**
   * 
   * @param {*} userKey 
   * @param {*} key 
   * @returns 
   */
  facade.findOne = async (userKey, key) => {
    const path = `users/${userKey}/${collectionName}`;
    const result = await findOne(path, key);
    return result;
  };

  /**
   * 
   * @param {*} userKey 
   * @param {*} document 
   * @returns 
   */
  facade.insertOne = async (userKey, document) => {
    const path = `users/${userKey}/${collectionName}`;
    const result = await insertOne(path, document);
    return result;
  };

  /**
   * 
   * @param {*} userKey 
   * @param {*} key 
   * @param {*} document 
   * @returns 
   */
  facade.replaceOne = async (userKey, key, document) => {
    const path = `users/${userKey}/${collectionName}`;
    const result = await replaceOne(path, key, document);
    return result;
  };

  /**
   * 
   * @param {*} userKey
   * @param {*} key 
   * @returns 
   */
  facade.deleteOne = async (userKey, key) => {
    const path = `users/${userKey}/${collectionName}`;
    const result = await deleteOne(path, key);
    return result;
  };

  return facade;
}

const facade = ProductFacade();
export default facade;
