import {
  findAll,
  findOne,
  replaceOne,
  deleteOne,
  insertOneWithKey
} from './facade-template.js';

function UserFacade() {
  const facade = {};
  const collectionName = 'users';

  /**
   * 
   * @returns 
   */
  facade.findAll = async () => {
    const result = await findAll(collectionName);
    return result;
  };

  /**
   * 
   * @param {*} key 
   * @returns 
   */
  facade.findOne = async (key) => {
    const result = await findOne(collectionName, key);
    return result;
  };

  /**
   * 
   * @param {*} key 
   * @returns 
   */
  facade.insertOneWithKey = async (key, document) => {
    const result = await insertOneWithKey(collectionName, key, document);
    return result;
  };

  /**
   * 
   * @param {*} key 
   * @param {*} document 
   * @returns 
   */
  facade.replaceOne = async (key, document) => {
    const result = await replaceOne(collectionName, key, document);
    return result;
  };

  
  /**
   * 
   * @param {*} key 
   * @returns 
   */
  facade.deleteOne = async (key) => {
    const result = await deleteOne(collectionName, key);
    return result;
  };

  return facade;
}

const facade = UserFacade();
export default facade;
