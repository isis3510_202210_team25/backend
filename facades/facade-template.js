import { db } from '../lib/firebaselib.js';
import {
  collection,
  doc,
  getDocs,
  getDoc,
  addDoc,
  updateDoc,
  deleteDoc,
  setDoc,
  query,
  where,
} from 'firebase/firestore';

class FacadeTemplate {

  /**
   * 
   * @param {*} path 
   * @returns 
   */
  findAll = async (path) => {
    try {
      const collectionRef = collection(db, path);
      const querySnapshot = await getDocs(collectionRef);
      const list = [];

      querySnapshot.forEach((doc) => {
        const data = doc.data();
        data.key = doc.id;
        list.push(data);
      });
    
      return {
        'data': list,
      };
    } catch (e) {
      console.error('Error retrieving page: ', e);
      return null;
    }
  };

  /**
   * 
   * @param {*} path 
   * @param {*} parentKeyField 
   * @param {*} parentKeyValue 
   * @returns 
   */
  findAllByParent = async (path, parentKeyField, parentKeyValue) => {
    const collectionRef = collection(db, path);
    const q = query(collectionRef, where(parentKeyField, '==', parentKeyValue));
    const querySnapshot = await getDocs(q);
    const list = [];

    querySnapshot.forEach((doc) => {
      const data = doc.data();
      data.key = doc.id;
      list.push(data);
    });

    return {
      'data': list,
    };
  };

  /**
   * 
   * @param {*} path 
   * @param {*} key 
   * @returns 
   */
  findOne = async (path, key) => {
    const docRef = doc(db, path, key);
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
      const data = docSnap.data();
      data.key = docSnap.id;
      return data;
    }

    return null;
  };

  /**
   * 
   * @param {*} path 
   * @param {*} document 
   * @returns 
   */
  insertOne = async (path, document) => {
    try {
      const collectionRef = collection(db, path);
      const docRef = await addDoc(collectionRef, document);
      const docSnap = await getDoc(docRef);

      if (docSnap.exists()) {
        const data = docSnap.data();
        data.key = docSnap.id;
        return data;
      }

      // Defaults to null
      return null;
    } catch (e) {
      console.error('Error adding focument: ', e);
      return null;
    }
  };

  /**
   * 
   * @param {*} path 
   * @param {*} key 
   * @param {*} document 
   * @returns 
   */
  insertOneWithKey = async (path, key, document) => {
    try {
      await setDoc(doc(db, path, key), document);
      document.key = key;
      return document;
    } catch(e) {
      console.error('Error adding document: ', e);
      return null;
    }
  };

  /**
   * 
   * @param {*} collectionName 
   * @param {*} key
   * @param {*} document 
   * @returns 
   */
  replaceOne = async (path, key, document) => {
    try {
      const docRef = doc(db, path, key);
      await updateDoc(docRef, document);

      document.key = key;
      return document;
    } catch (e) {
      console.error('Error updating focument: ', e);
      return null;
    }
  };

  /**
   * 
   * @param {*} path 
   * @param {*} key 
   * @returns 
   */
  deleteOne = async (path, key) => {
    try {
      const docRef = doc(db, path, key);
      await deleteDoc(docRef);
      return { key };
    } catch (e) {
      console.error('Error deleting document: ', e);
      return null;
    }
  };
}

const template = new FacadeTemplate();
export const findAll = template.findAll;
export const findAllByParent = template.findAllByParent;
export const findOne = template.findOne;
export const insertOne = template.insertOne;
export const insertOneWithKey = template.insertOneWithKey;
export const replaceOne = template.replaceOne;
export const deleteOne = template.deleteOne;
