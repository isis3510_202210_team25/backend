import express from 'express';
import cookieParser from 'cookie-parser';
import logger from 'morgan';

import indexRouter from './routes/api/index.js';
import usersRouter from './routes/api/users.js';
import devicesRouter from './routes/api/devices.js';
import productsRouter from './routes/api/products.js';
import groupsRouter from './routes/api/groups.js';
import consumptionsRouter from './routes/api/consumptions.js';
import nestedRouter from './routes/api/nested.js';

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/api/', indexRouter);
indexRouter.use('/users/', usersRouter);
usersRouter.use('/:userKey/devices/', devicesRouter);
usersRouter.use('/:userKey/products/', productsRouter);
usersRouter.use('/:userKey/groups/', groupsRouter);
usersRouter.use('/:userKey/consumptions/', consumptionsRouter);
usersRouter.use('/:userKey/', nestedRouter);

export default app;
