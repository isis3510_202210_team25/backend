import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';

// Import the functions you need from the SDKs you need
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyAAQh4hdzJxpQyfwS3hR0nW9MmS73IV8TA',
  authDomain: 'freeazy-a2687.firebaseapp.com',
  projectId: 'freeazy-a2687',
  storageBucket: 'freeazy-a2687.appspot.com',
  messagingSenderId: '763926368996',
  appId: '1:763926368996:web:3ea0efaf8f77554a43d2f1',
  measurementId: 'G-Y668KRW78K'
};
  
// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
