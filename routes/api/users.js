/* eslint-disable no-unused-vars */
import { Router } from 'express';
const router = Router();

import user from '../../facades/user-facade.js';

const notFoundMessage = 'The user with the given ID was not found.';

/**
 * 
 */
router.get('/', async (req, res, next) => {
  const result = await user.findAll();
  res.json(result);
});

/**
 * 
 */
router.post('/:key/',async function (req, res, next) {
  const key = req.params.key;

  const {
    name,
    birth,
  } = req.body;

  const result = await user.insertOneWithKey(key, {
    name,
    birth,
  });

  res.send(result);
});

/**
 * 
 */
router.get('/:key/', async function (req, res, next) {
  const key = req.params.key;

  const result = await user.findOne(key);

  if (result == null)
    return res.status(404).send(notFoundMessage);
  res.send(result);
});

/**
 * 
 */
router.put('/:key/', async function (req, res, next) {
  const key = req.params.key;
  const result = await user.replaceOne(key, req.body);
  if (result == null)
    return res.status(404).send(notFoundMessage);
  res.send(result);
});

/**
 * 
 */
router.delete('/:key/', async function (req, res, next) {
  const key = req.params.key;
  const result = await user.deleteOne(key);
  if (result == null)
    return res.status(404).send(notFoundMessage);
  res.send(result);
});

export default router;
