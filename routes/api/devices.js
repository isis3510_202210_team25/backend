/* eslint-disable no-unused-vars */
import { Router } from 'express';
const router = Router({mergeParams: true});

import device from '../../facades/device-facade.js';

const notFoundMessage = 'The device with the given ID was not found.';

/**
 * 
 */
router.post('/', async function (req, res, next) {
  const userKey = req.params.userKey;

  const {
    brand,
    label,
  } = req.body;

  console.log('Inserting device to user with key', userKey);

  const result = await device.insertOne(userKey, {
    brand,
    label,
  });

  console.log('The obtained result is:', result);

  res.send(result);
});

/**
 * 
 */
router.get('/', async function (req, res, next) {
  const userKey = req.params.userKey;
  const result = await device.findAllByUser(userKey);
  res.json(result);
});

/** 
 * 
 */
router.get('/:key/', async function (req, res, next) {
  const userKey = req.params.userKey;
  const key = req.params.key;
  const result = await device.findOne(userKey, key);

  if (result == null)
    return res.status(404).send(notFoundMessage);
  res.send(result);
});

/** 
 * 
 */
router.put('/:key/', async function (req, res, next) {
  const userKey = req.params.userKey;
  const key = req.params.key;
  const result = await device.replaceOne(userKey, key, req.body);

  if (result == null)
    return res.status(404).send(notFoundMessage);
  res.send(result);
});

/** 
 * 
 */
router.delete('/:key/', async function (req, res, next) {
  const userKey = req.params.userKey;
  const key = req.params.key;
  const result = await device.deleteOne(userKey, key);

  if (result == null)
    return res.status(404).send(notFoundMessage);
  res.send(result);
});

export default router;
