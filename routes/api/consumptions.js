/* eslint-disable no-unused-vars */
import { Router } from 'express';
const router = Router({mergeParams: true});

import consumption from '../../facades/consumption-facade.js';

const notFoundMessage = 'The consumption with the given ID was not found.';

/**
 * 
 */
router.get('/', async function (req, res, next) {
  const userKey = req.params.userKey;
  const result = await consumption.findAllByUser(userKey);
  res.json(result);
});

/** 
 * 
 */
router.put('/:key', async function (req, res, next) {
  const userKey = req.params.userKey;
  const key = req.params.key;
  const result = await consumption.replaceOne(userKey, key, req.body);

  if (result == null)
    return res.status(404).send(notFoundMessage);
  res.send(result);
});

/** 
 * 
 */
router.delete('/:key', async function (req, res, next) {
  const userKey = req.params.userKey;
  const key = req.params.key;
  const result = await consumption.deleteOne(userKey, key);

  if (result == null)
    return res.status(404).send(notFoundMessage);
  res.send(result);
});

export default router;
