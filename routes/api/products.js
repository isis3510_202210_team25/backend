/* eslint-disable no-unused-vars */
import { Router } from 'express';
const router = Router({mergeParams: true});

import product from '../../facades/product-facade.js';

const notFoundMessage = 'The product with the given ID was not found';

/**
 * 
 */
router.get('/', async (req, res, next) => {
  const userKey = req.params.userKey;
  const result = await product.findAllByUser(userKey);
  res.json(result);
});

/** 
 * 
 */
router.get('/:key/', async function (req, res, next) {
  const userKey = req.params.userKey;
  const key = req.params.key;
  const result = await product.findOne(userKey, key);

  if (result == null)
    return res.status(404).send(notFoundMessage);
  res.send(result);
});

/** 
 * 
 */
router.put('/:key/', async function (req, res, next) {
  const userKey = req.params.userKey;
  const key = req.params.key;
  const result = await product.replaceOne(userKey, key, req.body);

  if (result == null)
    return res.status(404).send(notFoundMessage);
  res.send(result);
});

/** 
 * 
 */
router.delete('/:key/', async function (req, res, next) {
  const userKey = req.params.userKey;
  const key = req.params.key;
  const result = await product.deleteOne(userKey, key);

  if (result == null)
    return res.status(404).send(notFoundMessage);
  res.send(result);
});

export default router;
