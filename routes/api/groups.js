/* eslint-disable no-unused-vars */
import { Router } from 'express';
const router = Router({mergeParams: true});

import group from '../../facades/group-facade.js';

const notFoundMessage = 'The stored product with the given ID was not found.';

/** 
 * 
 */
router.put('/:key/', async function (req, res, next) {
  const userKey = req.params.userKey;
  const key = req.params.key;
  const result = await group.replaceOne(userKey, key, req.body);

  if (result == null)
    return res.status(404).send(notFoundMessage);
  res.send(result);
});

/** 
 * 
 */
router.delete('/:key/', async function (req, res, next) {
  const userKey = req.params.userKey;
  const key = req.params.key;
  const result = await group.deleteOne(userKey, key);

  if (result == null)
    return res.status(404).send(notFoundMessage);
  res.send(result);
});

export default router;
