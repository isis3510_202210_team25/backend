/* eslint-disable no-unused-vars */
import { Router } from 'express';
const router = Router({mergeParams: true,});

import product from '../../facades/product-facade.js';
import group from '../../facades/group-facade.js';
import consumption from '../../facades/consumption-facade.js';

// RELATION BETWEEN DEVICE AND PRODUCT

/**
 * 
 */
router.get('/devices/:deviceKey/products/', async (req, res, next) => {
  const userKey = req.params.userKey;
  const deviceKey = req.params.deviceKey;

  const result = await product.findAllByDevice(userKey, deviceKey);
  res.json(result);
});

/**
 * 
 */
router.post('/devices/:deviceKey/products/', async function (req, res, next) {
  const userKey = req.params.userKey;
  const deviceKey = req.params.deviceKey;
  
  const {
    name,
    description,
    calories,
    image,
  } = req.body;

  const result = await product.insertOne(userKey, {
    deviceKey,
    name,
    description,
    calories,
    image,
  });

  res.send(result);
});

// RELATION BETWEEN PRODUCT AND GROUP

/**
 * 
 */
router.get('/products/:productKey/groups/', async (req, res, next) => {
  const userKey = req.params.userKey;
  const productKey = req.params.productKey;

  const result = await group.findAllByProduct(userKey, productKey);
  res.json(result);
});

/**
 * 
 */
router.post('/products/:productKey/groups/', async function (req, res, next) {
  const userKey = req.params.userKey;
  const productKey = req.params.productKey;
  
  const {
    addedAt,
    expiresAt,
    remainingUnits,
  } = req.body;

  const result = await group.insertOne(userKey, {
    productKey,
    addedAt,
    expiresAt,
    remainingUnits,
  });

  res.send(result);
});

// RELATION BETWEEN PRODUCT AND CONSUMPTION

/**
 * 
 */
router.get('/groups/:groupKey/consumptions/', async (req, res, next) => {
  const userKey = req.params.userKey;
  const groupKey = req.params.groupKey;

  const result = await consumption.findAllByGroup(userKey, groupKey);
  res.json(result);
});

/**
 * 
 */
router.post('/groups/:groupKey/consumptions/', async function (req, res, next) {
  const userKey = req.params.userKey;
  const groupKey = req.params.groupKey;
  
  const {
    at,
    units,
  } = req.body;

  const result = await consumption.insertOne(userKey, {
    groupKey,
    at,
    units
  });

  res.send(result);
});

export default router;
